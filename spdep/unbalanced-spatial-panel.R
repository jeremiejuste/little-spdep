options(zero.policy=TRUE)

m.coerce..into..data.table..and..sort..by..time..ind <-
    defmacro(expr={
        if (any(eff=="")) keys <- NULL else keys <- eff
        data <- setkeyv(as.data.table(data),keys)
    })

m.cbind..w..response..to..data <-
    defmacro(expr={        
        lag.listw..by..time <- function(response,time){
            if(res..balancedp) name <- "1" else  name <- as.character(time)
            
            spdep::lag.listw(list..listw[[name]],response)
        }

        response <- get..response(formula)
        w..response <- paste0("w..",response)
        
        data[,(w..response):=lag.listw..by..time(get(response),time),time]
    })

m.cbind..lag..explanatory..to..data..maybe <-
    defmacro(expr={
        create_WX..t <- function(x,time){
            name <- as.character(unique(time))
            if(res..balancedp){
                name <- "1"
            }
            
            matrix2list(spdep::create_WX(x,list..listw[[name]],prefix="lag",zero.policy=zero.policyp ))
        }
        if(durbinp){        
            explanatory <- get..explanatory(formula)
            lag..explanatory <- paste0("lag.",explanatory)            
            data[,(lag..explanatory):=create_WX..t(.SD,time),time,.SDcols=explanatory]
        }
    })

m.cbind..mean..explantory..to..data..maybe <- defmacro(expr={
    if(mundlakp){
        
        explanatory..var..names <- get..explanatory(formula)
        bar..names <- (paste0(explanatory..var..names,"..bari.")) 
        data[,(bar..names):=lapply(.SD,mean),ind]
        bar..names <- (paste0(explanatory..var..names,"..bar.t"))
        data[,(bar..names):=lapply(.SD,mean),time]
    }
    if(mundlak..notimep){
        explanatory..var..names <- get..explanatory(formula)
        bar..names <- (paste0(explanatory..var..names,"..bari.")) 
        data[,(bar..names):=lapply(.SD,mean),ind]
  
    }
})



m.add..lag..to..formula..maybe <- defmacro(expr={    
    if(durbinp) formula <- add..lag(formula) 
})

m.add..effect..to..formula..maybe <- defmacro(expr={    
    if(any(eff!="")) formula <- add(formula,eff,rep(TRUE,length(eff)))
})


m.add..mundlak..to..formula..maybe <- defmacro(
    expr={    
        if(mundlakp) {

            formulatmp <- formula
            formula <- add(formula,paste0(get..explanatory(formulatmp),"..bari."),
                           rep(FALSE,(length(get..explanatory(formulatmp)))))

            formula <- add(formula,paste0(get..explanatory(formulatmp),"..bar.t"),
                           rep(FALSE,(length(get..explanatory(formulatmp)))))
            
            
            formula
        }
        if (mundlak..notimep){
            formulatmp <- formula
            formula <- add(formula,paste0(get..explanatory(formulatmp),"..bari."),
                           rep(FALSE,(length(get..explanatory(formulatmp)))))

            formula
        }
    })

m.input..check <- defmacro(expr={
    
### check that all input have the right class
    inputs <- c("formula","data","list..listw","durbinp","zero.policyp","eff","mundlakp","mundlak..notimep")
    classes <- c("formula", "data.frame","list","logical","logical","character","logical","logical")
    
    ## return classn or my..class
    
    Map(function(input,classn) m.check..expect(input,
                                               function(var) classpv(classn)(get(var)), 
                                               classn),
        input=inputs,classn=classes)
    m.catv("checking that all parameters inputs are of the right class \n")
    
### check that effect is c(time,ind) or ""
    time..and..effect..or..char0 <- function(var) or(var == c("time","ind"),var=="")
    m.check..expect(eff,time..and..effect..or..char0,TRUE)
    
    m.catv("checking that eff is '' or c('time','ind') \n")

### if mundlakp or  mundlak..notimep expect eff=""
    if(mundlakp|mundlak..notimep){
        eff..isempty <- function(var) var==""
        m.check..expect(eff,eff..isempty,TRUE)
    }

    
### check that time is a column if not add it
    if(!"time" %in% colnames(data)){
        warning("==> WARNING :no time index adding time column to data. If it is a balanced panel you need to set the name of the time index name to time in the table. I'm assuming cross section here.")
        data[,time:=1]
        m.catv("No time variables, added time = 1 \n")
    } else {
        m.catv("time variable found! \n")
    }
### check panel data
    if..balanced..panel..expect..same..nrows..of..weight..matrix..and..data <-
        function(var) {
            if(length(var)==1){
                res..balancedp <<- TRUE
                (length(var[[1]]$neighbours)==nrow(data)|length(var[[1]]$neighbours)==unique(data[,.N,time][,N]))
            } else {
                res..balancedp <<- FALSE
                TRUE
            }
        }

    m.check..expect(list..listw,
                    if..balanced..panel..expect..same..nrows..of..weight..matrix..and..data,
                    TRUE)

    
    
    m.catv(paste("checking for panel type balanced panel: ",res..balancedp,"\n"))
    
    

### check that list..listw is a named list according to time if not name it

    if (res..balancedp){
        names(list..listw) <- as.character(1)
    } else{
        names(list..listw) <- as.character(unique(data[,time]))
        
    } 


###
    m.check..expect(list..listw[[1]],function(x) inherits(x ,"listw"),TRUE)
    
### if..unbalanced..panel..expect..same..nrows..of..weight..matrix..t..and..cohorts
    
    if(length(list..listw)>1){
        lapply(unique(data[,time]),
               function(t){
                   
                   m.check..expect(t, function(t) nrow(data[time==t,])== length(list..listw[[as.character(t)]]$neighbours),TRUE)}
               )
    }


    
})



unbalanced..spatial..panel <-   function(formula, data, list..listw,
                                         durbinp, zero.policyp,eff="",method="eigen",
                                         mundlakp=FALSE,mundlak..notimep=FALSE){
### DD
    ## formula          . object of class formula  y ~ Xβ 
    ## data             . data.frame or data.table
    ## list..listw      . list of class listw must be same order as time in data if panel
    ## durbinp          . boolean TRUE|FALSE for SDM|SAR 
    ## zero..policy     . boolean TRUE, each obs must have neighbours
    ## eff              . NULL| c("ind","time") must in non null for panel
    ## method           . character string that is either "eigen" or "spam..update"
    ## mundlak          . use mundlak procedure time and cross
    ## mundlak..notimep . use mundlak prodecure only cross
### Purpose
    ## prepare data for unbalanced panel estimation. The estimation will allow for
    ## cross, balanced panel and unbalanced panel (be more precise here)

    ## input..check
    
    res..balancedp <- FALSE
    m.coerce..into..data.table..and..sort..by..time..ind()
    
    ## check if data is balanced output res..balancedp
    ## for more precision see macro
    m.input..check()
    
    
    m.cbind..w..response..to..data()
    m.cbind..mean..explantory..to..data..maybe()
    m.add..mundlak..to..formula..maybe()
    m.cbind..lag..explanatory..to..data..maybe()
       
    m.add..lag..to..formula..maybe()
    m.add..effect..to..formula..maybe()
            
    timeit(res..list..sse..rho..t,
           compute..SSE..rho..function..by..t(formula,data,res..balancedp))
    
    m.add..can.simp..and..symmetricp..and..method..attributes..to..listw()
    
    tmp <- cachep(list..listw,jacobians(list..listw))
   
    add..to..my..hash(list..listw,tmp)
    data <- data
    
    res..list..jacobian <- lapply(tmp, function(x) x[["jacobian"]])
    res..list..interval <- lapply(tmp, function(x) x[["interval"]])
    res..list..eigen <- lapply(tmp, function(x) x[["eigen"]])
    res..durbinp <- durbinp

    ## macro to return all variables with, (prefix="res..") in the function and specify a class..name
    m.output("spatial..model", add..var=c("data","list..listw","formula"))
}


m.add..can.simp..and..symmetricp..and..method..attributes..to..listw <- defmacro(
    expr = {
        name <- attr(list..listw,"cache..name")
        list..listw <- lapply(list..listw,function(x){
            attr(x,"symmetricp") <- djj..is.symmetric.nb(x$neighbours,force=FALSE)
            attr(x,"can.simp") <- djj..can.be.simmed(x,restrict..styles = c("W","S"))
            attr(x,"method") <- method
            x
        })
        attr(list..listw,"cache..name") <- name
        list..listw
        
    })


compute..SSE..rho..function..by..t <-
    function(formula, data,balancedp){
### DD
        ## formula . formula y~x
        ## data . data.table

### Purpose
        ## compute the concentrated sum of squared error
### Ref Geographical Analysis Pace & Barry 97
        
        base <- lm(formula,data)
        response <- get..response(formula)
        w..response <- paste0("w..",response)
        w..formula <- as.formula(paste(w..response ,"~",
                                       paste0(get..explanatory(formula,TRUE),collapse="+")))
        weight <- lm(w..formula,data)
        if(balancedp){
            
            interval <- seqList(c(1,nrow(data)),2)
        } else{
            interval <- seqList(myrange(c(1,cumsum(data[,.N,time][,N]))),2)
        }

        lapply(interval,function(x){        
            sse..base <- crossprod(base$residuals[x[1]:x[2]])
            sse..w <- crossprod(weight$residuals[x[1]:x[2]])
            sse..x <- crossprod(base$residuals[x[1]:x[2]],weight$residuals[x[1]:x[2]])    
            function(rho)  sse..base - 2 * rho * sse..x + rho^2 * sse..w            
        })
    }




c.compute..SSE..rho..function..by..t <- cmpfun(compute..SSE..rho..function..by..t)

jacobian..eigen <- function(listw) {
    ## call test used for listw
    ## add additional test if necessary
    if (attr(listw,"can.sim")) {
        
        eig <- eigen(djj..similar.listw_Matrix(listw), 
                     only.values = TRUE)$value                
    } else {
        
        eig <- spdep::eigenw(listw)
    }

    if (is.complex(eig)) {
        eig.range <- 1/range(Re(eig[which(Im(eig) == 0)]))
    } else {
        eig.range <- 1/range(eig)
    }

    interval <- c(eig.range[1] + .Machine$double.eps, 
                  eig.range[2] - .Machine$double.eps)
    
    jacobian <- function(coef){
        det <- ifelse(is.complex(eig),
                      Re(sum(log(1 - coef * eig))),        
                      sum(log(1 - coef * eig)))
        det
        
    }

    
    list(interval=interval,
         jacobian=jacobian,
         eigen=eig)
    
}

listw2U_spam <- function (lw) 
{
    
    0.5 * (lw + t(lw))
}


as.spam.listw <- function (listw) 
{
    if (requireNamespace("spam", quietly = TRUE)) {
        N <- length(listw$neighbours)
        W_sn <- spdep::listw2sn(listw)
        rpts <- as.integer(cumsum(c(1, spdep::card(listw$neighbours))))
        W <- new("spam", entries = W_sn$weights, colindices = W_sn$to, 
                 rowpointers = rpts, dimension = as.integer(c(N, N)))
        stopifnot(spam::validate_spam(W))
        return(W)
    }
    else stop("spam not available")
}

djj..similar.listw_spam <- function(listw) 
{

    
    nbsym <- attr(listw, "symmetricp")
    
    if (!nbsym) 
        stop("Only symmetric nb can yield similar to symmetric weights")
    if (attr(listw$weights, "mode") == "general") 
        if (!attr(listw$weights, "glistsym")) 
            stop("General weights must be symmetric")
    n <- length(listw$neighbours)
    if (n < 1) 
        stop("non-positive number of entities")
    sww <- as.spam.listw(listw)
    if (listw$style == "W") {
        sd <- attr(listw$weights, "comp")$d
        sd1 <- 1/(sqrt(sd))
        sdd <- diag.spam(sd, n, n)
        sdd1 <- diag.spam(sd1, n, n)
        sww1 <- sdd %*% sww
        res <- sdd1 %*% sww1 %*% sdd1
    }
    else if (listw$style == "S") {
        q <- attr(listw$weights, "comp")$q
        Q <- attr(listw$weights, "comp")$Q
        eff.n <- attr(listw$weights, "comp")$eff.n
        q1 <- 1/(sqrt(q))
        qq <- diag.spam(q, n, n)
        qq1 <- diag.spam(q1, n, n)
        ww0 <- (Q/eff.n) * sww
        ww1 <- qq %*% ww0
        sim0 <- qq1 %*% ww1 %*% qq1
        res <- (eff.n/Q) * sim0
    }
    else stop("Conversion not suitable for this weights style")
    res
}


    jacobian..spam..update <- function(listw)
    {
        ## call test used for listw
        ## add additional test if necessary
        
        if(attr(listw,"can.sim")) {
            csrw <- listw2U_spam(djj..similar.listw_spam(listw))
        } else {
            csrw <- as.spam.listw(listw)
        }

        

        Id <- spam::diag.spam(1, length(listw), length(listw))
        cchol <- spam::chol.spam((Id - 0.1 * csrw), pivot = "MMD")


        det..NgPeyton <- function(coef){
            spam::determinant.spam.chol.NgPeyton(
                      spam::update.spam.chol.NgPeyton(cchol, 
                      (Id - coef * csrw)),
                      logarithm = TRUE)$modulus
        }

        jacobian <- function(coef){
            ifelse(abs(coef) < sqrt(.Machine$double.eps),
                   0,                               
                   m.trycatch(2*det..NgPeyton(coef), NA))

        }
        list(interval= c(-1, 0.999),
             jacobian=jacobian)

    }


jacobians <- function(list..listw){
    
    ## apply jacobian method for each member of the list..listw
    
    lapply..8(list..listw,function(x) 
        get(paste0("jacobian..",attr(x,"method")))(x))
}

## djj..is.summetric.nb
djj..is.symmetric.nb <- function (nb, verbose = FALSE, force = FALSE) {
    if (!all(class(nb)=="nb"))
        stop("Not neighbours list")
    stopifnot(is.logical(verbose))
    nbsym <- attr(nb, "sym")
    if (!is.null(nbsym)) 
        res <- nbsym
    if (force || is.null(nbsym)) {
        res <- .Call("symtest", nb = nb, card = as.integer(card(nb)), 
                     verbose = as.logical(verbose), PACKAGE = "spdep")
    }
    if (!res && verbose) 
        cat("Non-symmetric neighbours list \n")
    res
}


djj..similar.listw_Matrix  <- 
    function (listw) {
        check..expect..listw(listw,
                             c("symmetricp","can.simp"))
        
        ww <- as(listw, "CsparseMatrix")
        if (listw$style == "W") {
            
            d <- attr(listw$weights, "comp")$d
            d1 <- 1/(sqrt(d))
            dd <- as(as(Diagonal(x = d), "symmetricMatrix"), "CsparseMatrix")
            dd1 <- as(as(Diagonal(x = d1), "symmetricMatrix"), "CsparseMatrix")
            ww1 <- dd %*% ww
            res <- dd1 %*% ww1 %*% dd1
        }
        else if (listw$style == "S") {
            q <- attr(listw$weights, "comp")$q
            Q <- attr(listw$weights, "comp")$Q
            eff.n <- attr(listw$weights, "comp")$eff.n
            q1 <- 1/(sqrt(q))
            qq <- as(as(Diagonal(x = q), "symmetricMatrix"), "CsparseMatrix")
            qq1 <- as(as(Diagonal(x = q1), "symmetricMatrix"), "CsparseMatrix")
            ww0 <- (Q/eff.n) * ww
            ww1 <- qq %*% ww0
            sim0 <- qq1 %*% ww1 %*% qq1
            res <- (eff.n/Q) * sim0
        }
        else stop("Conversion not suitable for this weights style")
        res
    }

djj..can.be.simmed <- function (listw,restrict..styles=c("W", "B", "C", "S", "U", "minmax")) {
### Data Definition
    ## restrict..styles of the test to the syles of the nb2listw  NULL (all)

    check..expect..listw(listw,"symmetricp") 
    if(!listw$style %in% restrict..styles) return(FALSE)
    if(attr(listw$weights, "mode") == "general")  return(attr(listw$weights,"glistsym"))
    TRUE
} 


check..expect..listw <- function(listw,tests){
    if (length(tests)==1) tests <- list(tests)
    
    
    symmetryp <-function() stopiffalse((!is.null(attr(listw,"symmetricp"))& classp("nb")(listw)),
                                       paste("nb class with attribute symmetricp is required got class",
                                             class(listw),"and attibute", attr(listw,"symmetricp")))
    
    can.simp <- function()  stopiffalse(listw$can.sim,paste("weights cannot be simmed but symmetryp:" ,
                                                            symmetryp()))
    
    lapply(tests,function(x) call(x) )
}


